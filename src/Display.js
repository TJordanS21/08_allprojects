import {Route, Switch} from "react-router-dom";
import HelloWorld from "./Components/01_HelloWorld/HelloWorld.js"
import Formular from "./Components/02_Formular/Formular.js"
import List from "./Components/03_List/List.js"
import ToDo from "./Components/04_ToDo/ToDo.js"
import SBB from "./Components/05_SBB/SBB.js"
import DeckOfCards from "./Components/06_DeckOfCards/DeckOfCards.js"
import Middleware from "./Components/07_Middleware/Middleware.js"
import Hooks from "./Components/08_Hooks/Hooks.js"

import logo from "./logo.svg";

export default function Display() {
    return (
        <div>
            <Switch>
                <Route exact path="/HelloWorld" component={HelloWorld}/>
                <Route exact path="/Formular" component={Formular}/>
                <Route exact path="/List" component={List}/>
                <Route exact path="/ToDo" component={ToDo}/>
                <Route exact path="/SBB" component={SBB}/>
                <Route exact path="/DeckOfCards" component={DeckOfCards}/>
                <Route exact path="/Middleware" component={Middleware}/>
                <Route exact path="/Hooks" component={Hooks}/>
                <Route path={'/'}>
                    <header>
                        <img src={logo} className="App-logo" alt="logo"/>
                        <h1>
                            Click a Project to look at above ;)
                        </h1>
                    </header>
                </Route>
            </Switch>
        </div>)
}