import {Link} from "react-router-dom";
import {NavLink} from "react-bootstrap";

export default function Navigation() {
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <NavLink><Link to={"/"} className="navbar-brand">Home</Link></NavLink>
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div className="navbar-nav">
                        <NavLink><Link className="nav-item nav-link active" to={"/HelloWorld"}>Hello
                            World </Link></NavLink>
                        <NavLink><Link className="nav-item nav-link active" to={"/Formular"}>Formular</Link></NavLink>
                        <NavLink><Link className="nav-item nav-link active" to={"/List"}>List</Link></NavLink>
                        <NavLink><Link className="nav-item nav-link active" to={"/ToDo"}>ToDo</Link></NavLink>
                        <NavLink><Link className="nav-item nav-link active" to={"/SBB"}>SBB</Link></NavLink>
                        <NavLink><Link className="nav-item nav-link active" to={"/DeckOfCards"}>Deck of cards</Link></NavLink>
                        <NavLink><Link className="nav-item nav-link active" to={"/Middleware"}>Middleware</Link></NavLink>
                        <NavLink><Link className="nav-item nav-link active" to={"/Hooks"}>Hooks</Link></NavLink>
                    </div>
                </div>
            </nav>
        </div>)
}