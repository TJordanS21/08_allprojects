import {Star, StarFill} from "react-bootstrap-icons";

const Stars = ({rating, onChange}) => {
    const stars = [];
    for (let count = 1; count <= 5; count++) {
        stars.push(<button onClick={() => onChange(count === rating ? 0 : count)}>
            {count <= rating ? <StarFill/> : <Star/>}
        </button>)
    }
    return stars

}
export default Stars;