export default function Example1() {
    const colors = ['red', 'green', 'blue']

    return (
        <div>
            <h1>Example 1</h1>
            <ul>
                {colors.map((color, idx) => <li key={'color-' + idx}>{color}</li>)}
            </ul>
        </div>
    )
}