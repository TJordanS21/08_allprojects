import {Button, Col, Container, ListGroup, Row} from "react-bootstrap";
import React, {useState} from 'react';

export default function Example6() {
    const [data, setData] = useState([
        {title: 'Honda CBR 1000'},
        {title: 'Ducati Monster 120s'}
    ]);
    const [data2, setData2] = useState([
        {title: 'KTM Superduke 1200'},
        {title: 'Triumph Triple Speed'},
        {title: 'Yamaha MT-01'}
    ]);
    const [fieldValue1, setFieldValue1] = useState(0);
    const [fieldValue2, setFieldValue2] = useState(0);

    function alertClicked1(bike, index) {
        setFieldValue1(index)
    }

    function alertClicked2(bike, index) {
        setFieldValue2(index)
    }

    function button1() {
        setData2([...data2, ...data]);
        setData([])
    }

    function button2() {
        setData2([...data2, {title: data[fieldValue1].title}]);
        data.splice(fieldValue1, 1)
    }

    function button3() {
        setData([...data, {title: data2[fieldValue2].title}]);
        data2.splice(fieldValue2, 1)
    }

    function button4() {
        setData([...data, ...data2]);
        setData2([])
    }

    function buttonDel1() {
        data.splice(fieldValue1, 1)
    }
    function buttonDel2() {
        data2.splice(fieldValue2, 1)
    }
    return (
        <div>
            <h1>Aufgabe 3, 4, 6 Afternoon</h1>
            <Container>
                <Row>
                    <Col>
                        <ListGroup as="ul">
                            <ul>{data.map((bike, index) =>
                                <ListGroup.Item as="li" action
                                                onClick={() => alertClicked1(bike, index)}>{bike.title}</ListGroup.Item>)}
                            </ul>
                        </ListGroup>
                        <br/>
                        <Button variant="danger" onClick={buttonDel1}>Delete Entry</Button><br/>
                    </Col>
                    <Col>
                        <Button variant="secondary" onClick={button1}>{'>>'}</Button><br/>
                        <Button variant="secondary" onClick={button2}>{'>'}</Button><br/>
                        <Button variant="secondary" onClick={button3}>{'<'}</Button><br/>
                        <Button variant="secondary" onClick={button4}>{'<<'}</Button>
                    </Col>
                    <Col>
                        <ListGroup as="ul">
                            <ul>{data2.map((bike, index) =>
                                <ListGroup.Item as="li" action
                                                onClick={() => alertClicked2(bike, index)}>{bike.title}</ListGroup.Item>)}
                            </ul>
                        </ListGroup>
                        <br/>
                        <Button variant="danger" onClick={buttonDel2}>Delete Entry</Button><br/>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}