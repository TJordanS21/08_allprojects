export default function Example2() {
    const data = [
        {title: 'red', red: 255, green: 0, blue: 0, html: '#FF0000'},
        {title: 'green', red: 0, green: 255, blue: 0, html: '#00FF00'},
        {title: 'blue', red: 0, green: 0, blue: 255, html: '#0000FF'}
    ]

    return (
        <div>
            <h1>Example 3</h1>
            <ul>{data.map((color, index) =>
                <li style={{color: color.html}}>{color.title + " (" + color.red + "," + color.green + "," + color.blue + ")"}</li>)}
            </ul>
        </div>
    )
}