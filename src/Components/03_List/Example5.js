import {ListGroup} from "react-bootstrap";

export default function Example5() {
    const data = [
        {title: 'Honda CBR 1000'},
        {title: 'Ducati Monster 120s'},
        {title: 'KTM Superduke 1200'},
        {title: 'Triumph Triple Speed'},
        {title: 'Yamaha MT-01'}
    ]

    function alertClicked(bike, index) {
        alert('You clicked ' + bike.title);
    }
    return (
        <div>
            <h1>Aufgabe 2 Afternoon</h1>
            <ListGroup as="ul">
                <ul>{data.map((bike, index) =>
                    <ListGroup.Item as="li" action onClick={() => alertClicked(bike, index)}>{bike.title}</ListGroup.Item>)}
                </ul>
            </ListGroup>
        </div>
    )
}