//import './App.css';
import ExampleBasic from "./ExampleBasic.js";
import Example1 from "./Example1.js";
import Example2 from "./Example2.js";
import Example3 from "./Example3.js";
import Example4 from "./Example4.js";
import Example5 from "./Example5.js";
import Example6 from "./Example6.js";
import Example7 from "./Example7.js";
import Like from "./Like.js";
import Stars from "./Star.js";
import {useState} from "react";

export default function List() {
    const [checked, setChecked] = useState(false);
    const [star, setStar] = useState(0);

    return (
        <div className="App">
            <Like checked={checked} onChange={setChecked}/>
            <hr/>
            <Stars rating={star} onChange={setStar}/>
            <hr/>
            <hr/>
            <hr/>
            <ExampleBasic/>
            <hr/>
            <Example1/>
            <hr/>
            <Example2/>
            <hr/>
            <Example3/>
            <hr/>
            <Example4/>
            <hr/>
            <h1>Afternoon</h1>
            <hr/>
            <Example5/>
            <hr/>
            <Example6/>
            <hr/>
            <Example7/>
            <hr/>
        </div>
    );
}

