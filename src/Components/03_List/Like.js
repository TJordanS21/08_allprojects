import {Heart, HeartFill} from "react-bootstrap-icons";

const Like = ({checked, onChange}) => {

    return (
        <button onClick={() => onChange(!checked)}>
            {checked ? <HeartFill/> : <Heart/>}
        </button>
    )

}
export default Like;