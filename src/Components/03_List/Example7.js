import {Button, ListGroup} from "react-bootstrap";
import React, {useState} from 'react';

export default function Example6() {
    const [data, setData] = useState([
        {title: 'Honda CBR 1000'},
        {title: 'Ducati Monster 120s'}
    ]);
    const [data2, setData2] = useState([
        {title: 'KTM Superduke 1200'},
        {title: 'Triumph Triple Speed'},
        {title: 'Yamaha MT-01'}
    ]);
    const [fieldValue1, setFieldValue1] = useState(0);
    const [fieldValue2, setFieldValue2] = useState(0);

    const [fieldValues1, setFieldValues1] = useState([]);
    const [fieldValues2, setFieldValues2] = useState([]);

    function alertClicked1(bike, index) {
        setFieldValue1(index)
        if (fieldValues1.includes(index)) {
            const list = fieldValues1.slice();
            list.splice(list.indexOf(index), 1);

            setFieldValues1(list);
        } else {
            setFieldValues1([...fieldValues1, index]);
            fieldValues1.sort((a, b) => a > b ? -1 : 0);
        }
    }

    function alertClicked2(bike, index) {
        setFieldValue2(index)
        if (!fieldValues2.includes(index)) {
            setFieldValues2([...fieldValues2, index]);
            fieldValues2.sort((a, b) => a > b ? -1 : 0);
        }
    }

    function button1() {
        setData2([...data2, ...data]);
        setData([])
        setFieldValues1([])
        setFieldValues2([])
    }

    function button2() {
        for (const item of fieldValues1) {
            console.log(item)
            setData2([...data2, {title: data[item].title}]);
        }
        for (const item of fieldValues1) {
            console.log(item)
            data.splice(item, 1);
        }
        setFieldValues1([])
        setFieldValues2([])
    }

    function button3() {
        for (const item of fieldValues2) {
            setData([...data, {title: data2[item].title}]);
            data2.splice(item, 1);
        }
        setFieldValues1([])
        setFieldValues2([])
    }

    function button4() {
        setData([...data, ...data2]);
        setData2([])
        setFieldValues1([])
        setFieldValues2([])
    }

    function buttonDel1() {
        data.splice(fieldValue1, 1)
        setFieldValues1([])
        setFieldValues2([])
    }

    function buttonDel2() {
        data2.splice(fieldValue2, 1)
        setFieldValues1([])
        setFieldValues2([])
    }

    return (
        <div className="container">
            <h1>Aufgabe 5 Afternoon</h1>

            <div className="row">
                <div className="col-sm-5">
                    <ListGroup as="ul">
                        <ul>{data.map((bike, index) =>
                            <ListGroup.Item as="li" action active={fieldValues1.includes(index)}
                                            onClick={() => alertClicked1(bike, index)}
                                            key={index}>{bike.title}</ListGroup.Item>)}
                        </ul>
                    </ListGroup>
                    <br/>
                    <Button variant="danger" onClick={buttonDel1}>Delete Entry</Button><br/>
                </div>
                <div className="col-sm-2">
                    <Button variant="secondary" onClick={button1}>{'>>'}</Button><br/>
                    <Button variant="secondary" onClick={button2}>{'>'}</Button><br/>
                    <Button variant="secondary" onClick={button3}>{'<'}</Button><br/>
                    <Button variant="secondary" onClick={button4}>{'<<'}</Button>
                </div>
                <div className="5">
                    <ListGroup as="ul">
                        <ul>{data2.map((bike, index) =>
                            <ListGroup.Item as="li" action
                                            onClick={() => alertClicked2(bike, index)}
                                            key={index}>{bike.title}</ListGroup.Item>)}
                        </ul>
                    </ListGroup>
                    <br/>
                    <Button variant="danger" onClick={buttonDel2}>Delete Entry</Button><br/>
                </div>
            </div>
        </div>
    )
}