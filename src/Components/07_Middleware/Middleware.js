import React, {useState} from 'react';
import {Col, Row} from "react-bootstrap";
import {RhymeData} from "./Repository";

export default function App() {
    const [text, setText] = useState('')
    const [searchWord, setSearchWord] = useState('')

    return (
        <div>
            <header className="p-3 mb-2 bg-info text-white">
                <h1 className="display-1">TODO-Liste</h1>
            </header>
            <main>
                <div className="p-5">
                    <form>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Word to rhyme</label>
                            <input type="text" className="form-control" value={text}
                                   onChange={(e) => setText(e.target.value)}/>
                        </div>
                        <button type="button" className="bg-info btn btn-primary"
                                onClick={() => setSearchWord(text)}>Rhyme
                        </button>
                    </form>
                    <br/><br/>
                    <RhymeData word={searchWord}>
                        {renderProps => (
                            <React.Fragment>
                                {renderProps.loading && <div>
                                    <div className="spinner-border" role="status">
                                        <span className="sr-only">Loading...</span>
                                    </div>
                                </div>}
                                {!renderProps.loading && renderProps.error &&
                                <div>{`Error: ${renderProps.error}`}</div>}
                                {!renderProps.loading && !renderProps.error && renderProps.someData &&
                                <div>{renderProps.someData.map(word => <Row
                                    key={word.word}><Col>{word.word}</Col></Row>)}</div>}
                            </React.Fragment>)}
                    </RhymeData>
                </div>
            </main>
        </div>
    );
}

