import '../../App.css';
import React, {useState} from 'react';
import HelloWorld2 from "./HelloWorld2.js";

export default function HelloWorld() {

    const [counter, setCounter] = useState(0);
    const [fieldValue, setFieldValue] = useState("");
    const [submitValue, setSubmitValue] = useState("");


    function incCounter() {
        console.log("Button was clicked!")
        console.log(counter)
        setCounter(counter + 1)
    }

    function handleSubmit(event) {
        setSubmitValue(fieldValue)
    }

    return (
        <div className="App">
            <header className="App-header">
                <HelloWorld2 name="Tyler"/>
                <input type="button" onClick={() => incCounter()} value="Add 1"/>
                <input type="button" onClick={() => decCounter(setCounter, counter)} value="Detract 1"/>
                <input type="button" onClick={() => {
                    console.log("Button was clicked!")
                    setCounter(0)
                }} value="reset counter"/>
                <input type="button" onClick={() => setCounter(0)} value="clear"/>
                <h3>Counter = {counter}</h3>
                <form>
                    <label>
                        Name:
                        {/*<input type="text" value={fieldValue}*/}
                        {/*       onChange={(e) => setFieldValue(e.target.value)} name="name"/>*/}
                        <input type="text"
                               value={fieldValue}
                               onChange={(e) => handleChange(e, setFieldValue)}/>
                    </label>
                    <input type="button" value="Submit" onClick={(e) => handleSubmit(e)}/>
                    <p> Your name is: {fieldValue}</p>
                    <p> After Submit: {submitValue.toUpperCase()}</p>
                    <FamilyMembers/>
                </form>
            </header>
            <main>

            </main>
        </div>
    );
}

function FamilyMembers() {
    let family = [
        {name: "Peter", age: 18},
        {name: "Petra", age: 16}
    ]
    const list = family.map((person, idx) => <p key={idx}>{person.name}</p>)
    return (
        <div>
            <h3>List of Family Members</h3>
            {list}
        </div>
    )
}

const decCounter = (setCounter, counter) => {
    console.log("Button was clicked!")
    setCounter(counter - 1)
}

function handleChange(event, setFieldValue) {
    setFieldValue(event.target.value);
}