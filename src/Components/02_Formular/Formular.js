import '../../App.css';
import React, {useState} from 'react';

export default function Display() {
    const [fieldValue, setFieldValue] = useState("");
    const [fieldValue2, setFieldValue2] = useState("");

    const handleChange = (event) => {
        setFieldValue(event.target.value);
    }
    const handleChange2 = (event) => {
        setFieldValue2(event.target.value);
    }
    const handleSubmit = (event) => {
        alert('A name was submitted: ' + fieldValue.toUpperCase());
        alert('A fruit was selected: ' + fieldValue2);

        event.preventDefault();
    }

    return (
        <form onSubmit={handleSubmit}>
            <label>
                Name:
                <input type="text" value={fieldValue} onChange={handleChange}/>
            </label><br/>
            <label>
                Lieblingsfrucht:
                <select value={fieldValue2} onChange={handleChange2}>
                    <option value="grapefruit">Grapefruit</option>
                    <option value="lime">Lime</option>
                    <option selected value="coconut">Coconut</option>
                    <option value="mango">Mango</option>
                </select>
            </label><br/>
            <label>
                <input
                    type="radio"
                    name="react-tips"
                    value="option1"
                    checked={true}
                    className="form-check-input"
                />
                Option 1
            </label><label>
            <input
                type="radio"
                name="react-tips"
                value="option2"
                checked={true}
                className="form-check-input"
            />
            Option 2
        </label>
            <label>
                <input
                    type="radio"
                    name="react-tips"
                    value="option3"
                    checked={true}
                    className="form-check-input"
                />
                Option 3
            </label>

            <input type="submit" value="Submit" onClick={handleSubmit}/>
        </form>
    );
}