import React, {useContext, useState} from "react";

export default function Component2(props) {
    const number = useContext(props.context);
    const [newNumber, setNewNumber] = useState(number);

    return (<div>
            <h4>Component 2: Count = {number}</h4>
            <div className="form-group">
                <label htmlFor="exampleInputEmail1">Set new value for count:</label>
                <input type="text" className="form-control" defaultValue={number}
                       onChange={(e) => setNewNumber(e.target.value)}/>
            </div>
            <button type="button" className="bg-info btn btn-primary"
                    onClick={() => props.dispatch({type: 'set', payload: newNumber})}>Update
            </button>
        </div>
    )
}