import React, {useReducer} from "react";
import Component1 from "./Component1";
import Component2 from "./Component2";
import Hooks2 from "./Hooks2";

export default function Hooks() {
    const initialState = {count: 0};
    const [state, dispatch] = useReducer(reducer, initialState);
    const MyContext = React.createContext(initialState);

    return (
        <div>
            <header className="p-3 mb-2 bg-info text-white">
                <h1 className="display-1">Hooks</h1>
            </header>
            <main>
                <div className="p-5">

                    <h1>Aufgabe 1</h1>
                    <h4>Counter = {state.count}</h4>
                    <button className="bg-info btn btn-primary" onClick={() => dispatch({type: 'decrement'})}>-</button>
                    <button className="bg-info btn btn-primary" onClick={() => dispatch({type: 'increment'})}>+</button>
                    <button className="bg-info btn btn-primary" onClick={() => dispatch({type: 'clear'})}>clear</button>

                    <br/><br/><br/>

                    <h1>Aufgabe 2</h1>
                    <Component1 state={state}/>
                    <br/>
                    <MyContext.Provider value={state.count}>
                        <Component2 context={MyContext} dispatch={dispatch}/>
                    </MyContext.Provider>

                    <br/><br/><br/>

                    <h1>Aufgabe 3</h1>
                    <Hooks2/>
                </div>
            </main>
        </div>
    )
}

function reducer(state, action) {
    switch (action.type) {
        case methodes.increment:
            return {count: state.count + 1};
        case methodes.decrement:
            return {count: state.count - 1};
        case methodes.clear:
            return {count: 0};
        case methodes.set:
            return {count: action.payload};
        default:
            throw new Error();
    }
}

const methodes = {
    increment: 'increment',
    decrement: 'decrement',
    clear: 'clear',
    set: 'set'
}