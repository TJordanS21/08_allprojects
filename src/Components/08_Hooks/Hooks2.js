import React, {useState} from 'react';
import {Card} from "react-bootstrap";

export default function Hooks2() {
    const [text, setText] = useState('')
    const [todos, setTodos] = useState([{title: "mein erstes todo", done: false},
        {title: "mein zweites todo", done: false}])

    function handleSubmit(event) {
        setTodos([...todos, {title: text, done: false}]);
    }

    function handleCheckbox(idx) {
        let copy = [...todos];
        console.log(!todos[idx].title)
        copy.splice(idx, 1, {title: todos[idx].title, done: !todos[idx].done});
        setTodos(copy);
    }

    function del(idx) {
        let copy = [...todos];
        copy.splice(idx, 1);
        setTodos(copy);
        // setTodos([...todos].filter((todo,i) => i !== idx) )
    }

    return (
        <div>
            <main>
                <form>
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Zu erledigen</label>
                        <input type="text" class="form-control" placeholder="ToDos bearbeiten" value={text}
                               onChange={(e) => setText(e.target.value)}/>
                    </div>
                    <button type="button" class="bg-info btn btn-primary"
                            onClick={handleSubmit}>hinzufügen
                    </button>
                </form>
                <br/>
                {todos.map((todo, idx) =>
                    <Card key={idx} style={{ width: '18rem' }}>
                        <Card.Body>
                            <Card.Title> {todo.title + ": "}</Card.Title>
                            <Card.Text>{todo.done === true ? " Finished" : " Still open"}</Card.Text>
                            <input type="checkbox" className="form-check-input" onChange={() => handleCheckbox(idx)}/>
                            <Card.Text><label>Check if finished</label></Card.Text>
                            <button type="button" class="btn btn-outline-danger"
                                    onClick={() => del(idx)}>delete
                            </button>
                        </Card.Body>
                    </Card>)}
            </main>
        </div>
    );
}
