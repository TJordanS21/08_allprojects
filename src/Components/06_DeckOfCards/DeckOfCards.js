import React, {useState, useEffect} from 'react';

export default function DeckOfCards() {
    const [deck, setDeck] = useState();
    const [card, setCard] = useState({image: '', value: '', suit: '', code: ''})
    const [pulledCards, setPulledCards] = useState([])
// const order = (value) => ["ACE", "2", "3", "4", "5", "6", "7", "8", "9", "10", "JACK", "QUEEN", "KING"].indexOf(value
// cards
//   .sort((c1,c2) => order(c1.value) - order(c2.value))

    useEffect(() => {
        fetchDeck()
        // eslint-disable-next-line
    }, [])

    async function fetchDeck() {
        fetch('https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1')
            .then(response => response.json())
            .then(deck => {
                setDeck(deck);
            })
    }

    async function fetchCard() {
        fetch('https://deckofcardsapi.com/api/deck/' + deck.deck_id + '/draw/?count=1')
            .then(response => response.json())
            .then(cards => {
                setPulledCards([...pulledCards, ...cards.cards])
                setCard({
                    ...cards.cards[0]
                });
            })
    }

    function sortCards(a, b) {
        const values = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'JACK', 'QUEEN', 'KING', 'ACE']
        if (values.indexOf(a.value) > values.indexOf(b.value)) {
            return 1
        }
        return -1
    }

    return (
        <div>
            <header className="p-3 mb-2 bg-info text-white">
                <h1 className="display-1">Deck of cards</h1>
            </header>
            <main>
                <div className="p-5">
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Draw a card &emsp;</label>
                        <button type="button" className="bg-info btn btn-primary"
                                onClick={fetchCard}>Draw
                        </button>
                    </div>
                    <h3>Drawn Card:</h3>
                    <img src={card.image} alt={card.image}/>
                    <br/><br/>
                    <h3>All drawn Cards:</h3>
                    {pulledCards.map(selCard => <img key={selCard.image} src={selCard.image} alt={selCard.image}/>)}
                    <br/><br/>
                    <h3>All drawn Cards in correct Order:</h3>
                    {["CLUBS", "DIAMONDS", "HEARTS", "SPADES"].map(suit =>
                        <div>{pulledCards
                            .filter(selCard => selCard.suit === suit)
                            .sort(sortCards)
                            .map(selCard => <img key={selCard.image} src={selCard.image} alt={selCard.image}/>)}</div>)}
                </div>
            </main>
        </div>
    );
}

