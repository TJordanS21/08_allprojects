import React, {useState} from 'react';
import {Col, Row} from "react-bootstrap";

export default function App() {
    const [text, setText] = useState('')
    const [todos, setTodos] = useState([{title: "mein erstes todo", done: false},
        {title: "mein zweites todo", done: false}])

    function handleSubmit(event) {
        setTodos([...todos, {title: text, done: false}]);
    }

    function handleCheckbox(idx) {
        let copy = [...todos];
        console.log(!todos[idx].title)
        copy.splice(idx, 1, {title: todos[idx].title, done: !todos[idx].done});
        setTodos(copy);
    }

    function del(idx) {
        let copy = [...todos];
        copy.splice(idx, 1);
        setTodos(copy);
    }

    return (
        <div className="container">
            <header class="p-3 mb-2 bg-info text-white">
                <h1 className="display-1">TODO-Liste</h1>
            </header>
            <main>
                <p></p>
                <form>
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Zu erledigen</label>
                        <input type="text" class="form-control" placeholder="Hausaufgaben bearbeiten" value={text}
                               onChange={(e) => setText(e.target.value)}/>
                    </div>
                    <button type="button" class="bg-info btn btn-primary"
                            onClick={handleSubmit}>hinzufügen
                    </button>
                </form>
                <br/>
                {todos.map((todo, idx) =>
                    <Row key={idx}>
                        <Col>
                            {todo.title + ": "} {todo.done === true ? " Finished" : " Still open"}
                        </Col>
                        <Col>
                            <input type="checkbox" className="form-check-input" onChange={() => handleCheckbox(idx)}/>
                            <label>Check if finished</label>
                        </Col>
                        <Col>
                            <button type="button" class="btn btn-outline-danger"
                                    onClick={() => del(idx)}>delete
                            </button>
                        </Col>
                    </Row>)}
            </main>
        </div>
    );
}
