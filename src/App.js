import Navigation from "./Navigation";
import Display from "./Display";

function App() {
    return (
        <div>
            <div>
                <Navigation/>
                <Display/>
            </div>
        </div>
    );
}

export default App;
